## Vega-and-Lily
Welcome to my GitHub repo! I hope you enjoy Vega and Lily's current features, but if not, just file an issue with `[FEATURE REQUEST]` and I'll look at it asap!
Thanks for using my bots!

# Notes
Please make sure that you add your discord name to features you add, as it'll be way easier to contact the Dev of the feature.
The server for the Hata Library is [here](https://discord.gg/3cH2r5d).
Here are the invite links for [Vega](https://discordapp.com/oauth2/authorize?client_id=643013885670064139&scope=bot&permissions=538307656) and [Lily](https://discordapp.com/oauth2/authorize?client_id=643013826152890378&scope=bot&permissions=538307656)

# Acknowledgements
• Proxy - Me! For actually want be productive and not be a useless piece of shit like normal

• HuyaneMatsu - The dev of the Hata Library, they helped me make what I have so far

from weakref import WeakKeyDictionary

from hata.events import CommandProcesser
from hata.parsers import EventHandlerBase

class MessageDeleteWaitfor(EventHandlerBase):
    __slots__=('waitfors',)
    __event_name__='message_delete'
    def __init__(self):
        self.waitfors=WeakKeyDictionary()

    async def __call__(self,client,message):
        try:
            event=self.waitfors[message.channel]
        except KeyError:
            return
        await event(client,message)

    append=CommandProcesser.append
    remove=CommandProcesser.remove

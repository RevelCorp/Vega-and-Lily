def key_manage_help():
    cmds = "'keygen(name,choice)' - Generates a random key, name is the name of the file \n'keyread(name)' - Reads a key from a file, name is the name of the file, you must have a file to read the key from \n'passkeygen(passwd)' - Generates a key from a password, as long as the password is the same, you'll always get the same key \n'encrypt(msg,key)' - Encrypts a string, msg is the string you want to encrypt, key is the key that you are using (e.g. a key from 'passkeygen') \n'decrypt(encryptmsg,key)' - Decrypts an encrypted string from this module, encryptmsg is the encrypted message from 'encrypt(msg,key)', key is the key you used to encrypt the message"
    print(cmds)
    return

def keygen(name,choice):
    from cryptography.fernet import Fernet
    if choice == False:
        f=open(name + '.key','wb')
        f.write(Fernet.generate_key())
        f.close()
        return 'Key has been written to the file: ' + name + '.key'
    elif choice == True:
        return Fernet.generate_key().decode()
    else:
        raise('Invalid input, choice can only be True or False')

def keyread(name):
    f = open(name + '.key', 'rb')
    key = f.read()
    f.close()
    return key

def passkeygen(passwd):
    import os
    from cryptography.hazmat.backends import default_backend
    from cryptography.hazmat.primitives import hashes
    from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
    import base64
    password_provided = passwd
    password = password_provided.encode()
    return base64.urlsafe_b64encode(PBKDF2HMAC(algorithm=hashes.SHA256(),length=32,salt=os.urandom(16),iterations=100000,backend=default_backend()).derive(password)).decode()

def encrypt(msg,key):
    from cryptography.fernet import Fernet
    msg = msg.encode()
    return Fernet(key).encrypt(msg).decode()

def decrypt(encryptmsg,key):
    from cryptography.fernet import Fernet
    return Fernet(key).decrypt(encryptmsg).decode()

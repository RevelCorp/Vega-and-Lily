from hata import eventlist

commands=eventlist()

@commands
async def convo(client, message, content):
	choice = random.randint(0, 2)
	if choice == 0:
		await Vega.message_create(message.channel, "How are you today Lily?")
		await Lily.typing(message.channel)
		await sleep(delay, client.loop)
		await Lily.message_create(
		    message.channel, "I'm fine sister, what are we going to do today?")
		await Vega.typing(message.channel)
		await sleep(delay, client.loop)
		await Vega.message_create(
		    message.channel,
		    "Idk, Maybe ask Proxy to make a new feature for us?")
		await Lily.typing(message.channel)
		await sleep(delay, client.loop)
		await Lily.message_create(message.channel,
		                          "But what feature should we ask for?")
		await Lily.typing(message.channel)
		await sleep(delay - 0.5, client.loop)
		await Lily.message_create(message.channel,
		                          "I mean, we can't just ask for anything")
		await Vega.typing(message.channel)
		await sleep(delay, client.loop)
		await Vega.message_create(
		    message.channel,
		    "True... Welp I'ma think about that for a while now")
		await Lily.typing(message.channel)
		await sleep(delay, client.loop)
		await Lily.message_create(message.channel, "I will too!")
	elif choice == 1:
		await Lily.message_create(message.channel, "Sister, guess what!")
		await Vega.typing(message.channel)
		await sleep(delay, client.loop)
		await Vega.message_create(
		    message.channel,
		    "What is it sister? Did you figure out how to ~~steal the souls of the innocent~~ make sweets that taste good for once?"
		)
		await Lily.typing(message.channel)
		await sleep(delay + 1, client.loop)
		await Lily.message_create(
		    message.channel,
		    "I just found out that I can dance like there's no tomorrow!")
		await Lily.typing(message.channel)
		await sleep(delay, client.loop)
		await Lily.message_create(
		    message.channel, "***Dances like crazy, I need a gif for this***")
		await Vega.typing(message.channel)
		await sleep(delay, client.loop)
		await Vega.message_create(message.channel,
		                          "Wow... That is one stupid dance...")
		await Lily.typing(message.channel)
		await sleep(delay, client.loop)
		await Lily.message_create(message.channel, "TwT")
	elif choice == 2:
		await Vega.message_create(
		    message.channel, "Sister, we need to do something fun one day")
		await Lily.typing(message.channel)
		await sleep(delay, client.loop)
		await Lily.message_create(message.channel, "Like what?")
		await Vega.typing(message.channel)
		await sleep(delay, client.loop)
		await Vega.message_create(
		    message.channel, "I don't know, but we need to do something!")
		await Lily.typing(message.channel)
		await sleep(delay, client.loop)
		await Lily.message_create(message.channel,
		                          "Well that's very useful now, isn't it?")
		await Vega.typing(message.channel)
		await sleep(delay, client.loop)
		await Vega.message_create(message.channel, "I kNoE rIgHt?")
	else:
		await Lily.message_create(message.channel,
		                          f"No output with convo {choice}")
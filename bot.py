import subprocess, random, os, requests, asyncio, json, threading, pickle as pkl

try:
    import hata
    import flask
    del(hata, flask)
except:
    os.system('pip3 install https://github.com/HuyaneMatsu/hata/archive/master.zip flask')

from hata import Client, start_clients, stop_clients, events, sleep, Embed, ActivityGame, CHANNELS, USERS, enter_executor
from hata.events import Cooldown, ReactionAddWaitfor, ReactionDeleteWaitfor, ContentParser, Pagination
from hata.extension_loader import ExtensionLoader, ExtensionError

from flask import Flask, request

from vlmodules.interpreter import Interpreter
from vlmodules import channeller
from vlmodules.key_manage import *
from vlmodules import tools

app = Flask(__name__)

with open(".env") as f:
    clients=json.load(f)

Lily = Client(clients["Lily"], client_id=0)
Vega = Client(clients["Vega"], client_id=1)

lon_command = Lily.events(events.CommandProcesser('l/')).shortcut
Lily.events(ReactionAddWaitfor)
Lily.events(ReactionDeleteWaitfor)
Lily.events(tools.MessageDeleteWaitfor)
lon_command(channeller.channeling_start)
lon_command(channeller.channeling_stop)

von_command = Vega.events(events.CommandProcesser('v/')).shortcut
Vega.events(ReactionAddWaitfor)
Vega.events(ReactionDeleteWaitfor)

delay = 1.25
OIDs = [524288464422830095, 562086061153583122]

async def handler(client, message, command, time_left):
	await client.message_create(
	    message.channel,
	    f"You're on cool down, please wait for {ceil(time_left)} seconds to use the command again!"
	)

async def status():
	lstatuses = ["the goof off game with Vega", "Just Dance 2020 with Vega"]
	vstatuses = ["the goof off game with Lily", "Just Dance 2020 with Lily"]
	while True:
		await Lily.client_edit_presence(
		    activity=ActivityGame.create(f'{lstatuses[0]}'), status='online')
		await Vega.client_edit_presence(
		    activity=ActivityGame.create(f'{vstatuses[0]}'), status='online')
		await sleep(12, Vega.loop)
		await Lily.client_edit_presence(
		    activity=ActivityGame.create(f'{lstatuses[1]}'))
		await Vega.client_edit_presence(
		    activity=ActivityGame.create(f'{vstatuses[1]}'))
		await sleep(12, Lily.loop)

@Lily.events
class once_on_ready(object):
	__slots__ = ('called', )
	__event_name__ = 'ready'

	def __init__(self):
		self.called = False

	async def __call__(self, client):
		if self.called:
			return

		self.called = True
		print(f'{Lily:f} has been started')
		client.loop.create_task(status())


@Vega.events
class once_on_ready(object):
	__slots__ = ('called', )
	__event_name__ = 'ready'

	def __init__(self):
		self.called = False

	async def __call__(self, client):
		if self.called:
			return

		self.called = True
		print(f'{Vega:f} has been started')

def cmds(v_or_l, message):
    members, uid, perms = message.guild.users, message.author.id, list(message.channel.cached_permissions_for(v_or_l))
    ignore=[]
    if uid not in OIDs:
        ignore.append('execute')
    if (Vega and Lily) not in message.guild.clients:
        for X in ['rps', 'convo', 'bored']:
            ignore.append(X)
    if 'manage_webhooks' not in perms:
        ignore.append('channeling_start')
        ignore.append('channeling_stop')
    commands = []
    if v_or_l == Vega:
        for command in Vega.events.message_create.commands:
            if command.lower() not in ignore:
                commands.append(command)
    elif v_or_l == Lily:
        for command in Lily.events.message_create.commands:
            if command not in ignore:
                commands.append(command)
    else:
    	return 'error'
    return commands

def help_pages(cmds):
    pages=[]
    part=[]
    index=0
    for element in cmds:
        if index==16:
            pages.append('\n'.join(part))
            part.clear()
            index=0
        part.append(f'**>>** {element}')
        index+=1

    pages.append('\n'.join(part))

    del part

    result=[]

    limit=len(pages)
    index=0
    while index<limit:
        embed=Embed('Commands:',color='029320',description=pages[index])
        index+=1
        embed.add_field(" ᠌ ᠌᠌ ᠌ ᠌ ᠌", f'page {index}/{limit}')
        result.append(embed)

    del pages
    return result

@von_command
async def help(client, message):
    await Pagination(client, message.channel, help_pages(cmds(client, message)))

@lon_command
async def help(client, message):
    await Pagination(client, message.channel, help_pages(cmds(client, message)))
    
@lon_command
async def meme(client, message):
    try:
        with open('./config/meme.json') as f:
            data=json.load(f)
        embed=Embed(data['title']).add_image(data['url'])
        await client.message_create(message.channel,embed=embed)
        async with enter_executor():
            with open('./config/meme.json', 'w+') as f:
                r=requests.get('https://meme-api.herokuapp.com/gimme')
                json.dump(json.loads(r.text), f)
    except:
        async with enter_executor():
            r = requests.get('https://meme-api.herokuapp.com/gimme')
            with open('./config/meme.json', 'w+') as f:
                json.dump(json.loads(r.text), f)

@von_command(case='bored')
async def im_bored(client, message, content):
	await Vega.message_create(message.channel, "Awww, are you bored?")
	await Vega.typing(message.channel)
	await sleep(delay - 0.25, client.loop)
	await Vega.message_create(message.channel,
	                          "I don't care! You shall suffer!")
	await Lily.typing(message.channel)
	await sleep(delay, client.loop)
	await Lily.message_create(message.channel,
	                          "That's rude <@643013885670064139>!")
	await Vega.typing(message.channel)
	await sleep(delay, client.loop)
	await Vega.message_create(message.channel, "Sorry....")


@lon_command(case='rps')
async def rockpaperscissors(client, message):
	rps = ['Rock', 'Paper', 'Scissors']
	chancel = rps[random.randint(0, 2)]
	chancev = rps[random.randint(0, 2)]
	await Lily.message_create(message.channel, "Rock...")
	await Vega.typing(message.channel)
	await sleep(delay - 0.25, client.loop)
	await Vega.message_create(message.channel, "Paper")
	await Lily.typing(message.channel)
	await sleep(delay - 0.25, client.loop)
	await Lily.message_create(message.channel, "Scissors")
	await Lily.typing(message.channel)
	await Vega.typing(message.channel)
	await sleep(delay - 0.25, client.loop)
	await Lily.message_create(message.channel, "Shoot!")
	await Vega.message_create(message.channel, "Shoot!")
	if chancel == chancev:
		await Lily.message_create(message.channel,
		                          f"Damn, we both got {chancel}")
		await Vega.typing(message.channel)
		await sleep(delay - 0.25, client.loop)
		await Vega.message_create(message.channel, "Hmph")
	elif chancel == 'Paper' and chancev == 'Rock' or chancel == 'Rock' and chancev == 'Scissors' or chancel == 'Scissors' and chancev == 'Paper':
		await Lily.message_create(
		    message.channel,
		    f"Ha! I won! I got {chancel} and you got {chancev}!")
		await Vega.typing(message.channel)
		await sleep(delay - 0.25, client.loop)
		await Vega.message_create(message.channel, "Cheater! You cheated!")
		await Lily.typing(message.channel)
		await sleep(delay - 0.25, client.loop)
		await Lily.message_create(message.channel,
		                          "Awwww, someone grumpy that they lost?")
	elif chancev == 'Paper' and chancel == 'Rock' or chancev == 'Rock' and chancel == 'Scissors' or chancev == 'Scissors' and chancel == 'Paper':
		await Vega.message_create(
		    message.channel,
		    f"Ha! I won! I got {chancev} and you got {chancel}")
		await Lily.typing(message.channel)
		await sleep(delay - 0.25, client.loop)
		await Lily.message_create(message.channel, "Good job sis!")
		await Vega.typing(message.channel)
		await sleep(delay - 0.25, client.loop)
		await Vega.message_create(message.channel, "Thanks Lily!")
	else:
		await Vega.message_create(
		    message.channel, "Indeterminable outcome, please try again...")


@lon_command
async def invite(client, message):
	await Lily.message_create(
	    message.channel,
	    "Thanks for inviting me!\nhttps://discordapp.com/oauth2/authorize?client_id=643013826152890378&scope=bot&permissions=538307656"
	)

@von_command
async def invite(client, message):
	await Vega.message_create(
	    message.channel,
	    "Thank you! Now I can annoy Lily that I am in more servers then her!\nhttps://discordapp.com/oauth2/authorize?client_id=643013885670064139&scope=bot&permissions=538307656"
	)

@von_command
async def system(client, message, content):
	if message.author.id in OIDs:
		await Vega.message_create(message.channel,
		                          "Okay, let me just carry out the command!")
		await Vega.typing(message.channel)
		async with enter_executor(): output = subprocess.getoutput(content)
		await Vega.message_create(message.channel,
		                          f"Done! Here's the output!```{output}```")
	else:
		await Vega.message_create(message.channel, "Okay just-")
		await Lily.typing(message.channel)
		await sleep(delay - 0.75, client.loop)
		await Lily.message_create(
		    message.channel,
		    "Sister wait! They aren't Proxy! We can't allow them to run any commands on their system!"
		)
		await Vega.typing(message.channel)
		await sleep(delay - 0.15, client.loop)
		await Vega.message_create(message.channel,
		                          "Oh... I didn't realize, thanks sis!")


@von_command
async def say(client, message, content):
	await client.message_delete(message)
	if message.author.id == 524288464422830095:
		await Vega.message_create(message.channel, content)
	else:
		await Vega.message_create(message.channel,
		                          f"{message.author} said: {content}")


@lon_command
async def say(client, message, content):
	await client.message_delete(message)
	if message.author.id == 524288464422830095:
		await Lily.message_create(message.channel, content)
	else:
		await Lily.message_create(message.channel,
		                          f"{message.author} said: {content}")


@von_command(case='create-hook')
async def webhook_create(client, message, content):
	if message.guild is None:
		return
	elif not message.guild.permissions_for(
	    message.
	    author).can_manage_webhooks or not message.channel.permissions_for(
	        message.author).can_manage_webhooks:
		Vega.message_create(
		    message.channel,
		    "You do not have the `Manage webhooks` or `administrator` permission"
		)
		return
	if content == '':
		content = 'VegaHook'
	else:
		pass
	cchannel = message.channel
	webhook = await client.webhook_create(message.channel, content)
	channel = await client.channel_private_create(message.author)
	message = await client.message_create(channel, webhook.url)
	await Vega.message_create(
	    cchannel,
	    "I've sent you a link with the webhook in a DM! If you didn't get it, you might need to allow DMs from server members"
	)

@von_command
@Cooldown('user', 20, handler=handler)
async def ping(client, message, content):
	await Vega.message_create(
	    message.channel,
	    f"Your ping is: {int(Vega.gateway.latency * 1000)} ms")

@von_command(case='randomkey')
async def randomkeymaker(client, message, content):
	await client.message_delete(message)
	cchannel = message.channel
	channel = await client.channel_private_create(message.author)
	await Vega.message_create(
	    cchannel,
	    "Sending you the key now, only share this with people who you want to be able to decrypt the message (That's been encrypted with this key"
	)
	await Vega.message_create(channel, '`' + keygen('key', True) + '`')


@von_command(case='passkey')
async def randomkeymaker(client, message, content):
	await client.message_delete(message)
	cchannel = message.channel
	channel = await client.channel_private_create(message.author)
	await Vega.message_create(
	    cchannel,
	    "Sending you the key now, only share this with people who you want to be able to decrypt the message (That's been encrypted with this key), even if you use the password key generator again with the same password, I will send you a different key each time, this is for security purposes, so keep this key safe (Aka I'm not broken, this is to ensure security)"
	)
	await Vega.message_create(channel, '`' + passkeygen(content) + '`')


@von_command(case='encrypt')
@ContentParser('str', 'rest')
async def msgencrypter(client, message, key, msg):
	await client.message_delete(message)
	cchannel = message.channel
	channel = await client.channel_private_create(message.author)
	try:
		await Vega.message_create(channel,
		                          '`' + encrypt(msg, key.encode()) + '`')
		await Vega.message_create(
		    cchannel, "Message has been encrypted! Please check your DMs")
	except:
		await Vega.message_create(
		    cchannel,
		    "There was an issue with encrypting your message, is the key valid?"
		)


@von_command(case='decrypt')
@ContentParser('str', 'rest')
async def msgdecrypter(client, message, key, msg):
	await client.message_delete(message)
	cchannel = message.channel
	channel = await client.channel_private_create(message.author)
	try:
		await Vega.message_create(
		    channel, '`' + decrypt(msg.encode(), key.encode()) + '`')
		await Vega.message_create(
		    cchannel, "Message has been decrypted! Please check your DMs")
	except:
		await Vega.message_create(
		    cchannel,
		    "There was an issue with decrypting your message, is the key valid?"
		)    

locals_=locals().copy()
lon_command(Interpreter(locals_), case='execute')
von_command(Interpreter(locals_), case='execute')
del locals_

start_clients()
'''
@app.route('/')
def index():
    return 'pong'

app.run('0.0.0.0')
'''
